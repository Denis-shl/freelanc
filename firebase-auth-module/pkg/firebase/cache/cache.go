package cache

import (
	"context"
	"time"
)

type value struct {
	time time.Time
	val  map[string]interface{}
}

type Cache struct {
	maps map[string]value
}

func (c *Cache) SetValue(key string, val map[string]interface{}) {
	var (
		value value
	)

	value.time = time.Now()
	value.val = val

	c.maps[key] = value
}

func (c *Cache) GetValue(key string) (val map[string]interface{}, ok bool) {
	var (
		value value
	)
	value, ok = c.maps[key]

	val = value.val
	return
}

func (c *Cache) DeleteValue() {

}

func (c *Cache) DeleteRottenValues(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			return

		}
	}
}

func NewCache() *Cache {
	return &Cache{
		maps: map[string]value{},
	}
}
