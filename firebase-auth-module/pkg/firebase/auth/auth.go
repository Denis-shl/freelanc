package auth

import (
	"bytes"
	"context"
	"encoding/gob"
	"fmt"
	"log"
	"time"

	"firebase.google.com/go/auth"
	"github.com/coocood/freecache"
)

type Config struct {
	ExpireSecond int

	CacheSize int

	TimeoutFirebase time.Duration

	Cli *auth.Client
}

type Auth struct {
	config Config

	cache *freecache.Cache
}

// Отдает мапу ролей которые доступны данному юзеру
// Используется https://github.com/coocood/freecache
// Время протухания кэша передается через конфиг
// https://firebase.google.com/docs/auth/admin/manage-users
func (a *Auth) GetClaims(ctx context.Context, uid string) (response map[string]interface{}, err error) {
	val, err := a.cache.Get([]byte(uid))
	if err == nil {
		var b bytes.Buffer
		_, err = b.Write(val)
		if err != nil {
			return nil, err
		}
		enc := gob.NewDecoder(&b)
		err = enc.Decode(&response)
		if err != nil {
			return nil, err
		}

		return
	} else {
		log.Println(err)
		err = nil
	}

	ctxFirebase, f := context.WithTimeout(ctx, time.Second*10)
	defer f()
	userRecord, err := a.config.Cli.GetUser(ctxFirebase, uid)

	if err != nil {
		return nil, fmt.Errorf("%v", firebaseNotFoundUser)
	}

	response = userRecord.CustomClaims

	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	err = enc.Encode(response)
	if err != nil {
		return nil, err
	}

	err = a.cache.Set([]byte(uid), b.Bytes(), a.config.ExpireSecond)
	if err != nil {
		log.Println(err) // TODO
	}

	return
}

func (a *Auth) SetClaims(ctx context.Context, uid string, claims map[string]interface{}) (err error) {
	ctxSetCustomUserClaims, f := context.WithTimeout(ctx, time.Second*a.config.TimeoutFirebase)
	defer f()

	err = a.config.Cli.SetCustomUserClaims(ctxSetCustomUserClaims, uid, claims)

	return
}

func NewAuth(config Config) *Auth {
	cache := freecache.NewCache(config.CacheSize)
	return &Auth{
		config: config,
		cache:  cache,
	}
}
