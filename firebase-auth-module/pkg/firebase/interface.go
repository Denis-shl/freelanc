package firebase

import "context"

type Auth interface {
	GetClaims(ctx context.Context, token string) (response map[string]interface{}, err error)
}
