module gitlab/firebase-auth-module

go 1.14

require (
	cloud.google.com/go/firestore v1.5.0 // indirect
	firebase.google.com/go v3.13.0+incompatible
	github.com/coocood/freecache v1.1.1
	google.golang.org/api v0.42.0
)
