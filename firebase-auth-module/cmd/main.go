package main

import (
	"context"
	firebase "firebase.google.com/go"
	"fmt"
	"google.golang.org/api/option"
	"log"
	"time"

	"gitlab/firebase-auth-module/pkg/firebase/auth"
)

func main() {
	opt := option.WithCredentialsFile("/Users/denis.shlomin/go/src/gitlab/firebase-auth-module/cmd/test/secret/test-65a28-firebase-adminsdk-zy0b3-452d302b85.json")
	app, err := firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		panic(err)
	}

	cli, err := app.Auth(context.Background())
	if err != nil {
		panic(err)
	}
	config := auth.Config{
		ExpireSecond: 600,
		Cli:          cli,
		CacheSize:    1024 * 1024 * 1024,
	}
	aut := auth.NewAuth(config)

	start2 := time.Now()
	ctx := context.Background()
	claims, err := aut.GetClaims(ctx, "d1xa4ZeW3gc5v865cPlEEWJLVbC3")
	if err != nil {
		log.Fatal(err)
	}
	duration2 := time.Since(start2)
	fmt.Println(duration2)
	fmt.Println(claims)

	start := time.Now()
	cla, err := aut.GetClaims(ctx, "d1xa4ZeW3gc5v865cPlEEWJLVbC3")
	if err != nil {
		log.Fatal(err)
	}
	duration := time.Since(start)
	fmt.Println(duration)
	fmt.Println(cla)
}
